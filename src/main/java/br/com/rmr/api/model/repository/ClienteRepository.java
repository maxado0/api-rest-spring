package br.com.rmr.api.model.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.rmr.api.model.entities.Cliente;

public interface ClienteRepository extends CrudRepository<Cliente, Long>{
	Cliente findByName(String name);
}
