package br.com.rmr.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.rmr.api.model.entities.Cliente;
import br.com.rmr.api.model.repository.ClienteRepository;

@RestController
@RequestMapping("cliente")
public class ClienteController {

	@Autowired
	private ClienteRepository clienteDao;
	
	@GetMapping
	public ResponseEntity<?> findAll() {	
		return new ResponseEntity<>( clienteDao.findAll(), HttpStatus.OK) ;
	}
	
	@GetMapping(path= "/{name}")
	public ResponseEntity<?> findByName(@PathVariable String name) {
		return new ResponseEntity<>(clienteDao.findByName(name), HttpStatus.OK) ;
	}
	
	@PostMapping
	public ResponseEntity<?> create(@RequestBody Cliente cliente) {
		return new ResponseEntity<>(clienteDao.save(cliente), HttpStatus.OK);
	}
	
	@PutMapping
	public ResponseEntity<?> update(@RequestBody Cliente cliente) {
		return new ResponseEntity<>(clienteDao.save(cliente), HttpStatus.OK);
	}
	
	@DeleteMapping(path = "/{id}")
	public ResponseEntity<?> delete(@PathVariable Long id){
		clienteDao.deleteById(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}
