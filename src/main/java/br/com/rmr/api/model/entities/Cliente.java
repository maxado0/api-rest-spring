package br.com.rmr.api.model.entities;
import javax.persistence.Entity;



@Entity
public class Cliente extends AbstractEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3693999278550968557L;
	
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	
}
